# wrf-runs-melissa-da

This repo provides the configurations of all the different runs that were performed to generate the charts for the 2024 article "Dynamic Load/Propagate/Store for Data Assimilation with Particle Filters on Supercomputers" and of several more runs.

Note that we pruned binary output and output log files to create a slim collection that shows how and where the experiments were run. The provided information is enough to reproduce our experiments. If you are interested in the pruned data (logs, binary output) please send a mail to the paper authors.



