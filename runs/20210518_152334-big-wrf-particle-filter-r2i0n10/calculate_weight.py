from melissa_wrf_helpers import *
from functools import lru_cache

import netCDF4 as nc

import time


def assimilation_cycle_to_date_str(ac):
    c = max(ac - 1, 0)  # take first obs file twice, even when it  makes no sense to even load obs for the very first cycle
    return "20180721%02d0000" % c   # ok we match with completely wrong observations for now!

def get_obs(assimilation_cycle):
    if not get_obs.d:
        ds = nc.Dataset('EU_TEST2_220x220_CLMask_20180719.nc')
        get_obs.d = ds['CLMask_P50']
        # ds.close()
    return get_obs.d[assimilation_cycle, :, :]
    # return np.random.random((220, 220))



    return cMa_rint

get_obs.d = None

# WIDTH=HEIGH in grid cells
WH = (220, 220)
last_cycle = -1
def calculate_weight(assimilation_cycle, pid, x_b, x_h, assimilated_index, assimilated_varid, fcomm):
    global last_cycle
    rank = -1
    try:
        comm = MPI.COMM_WORLD.f2py(fcomm)
        rank = comm.rank

        dprint('0')
        # takes too much memory if parallelized on all cores. lets just generate random data :P for debugging:
        obs = get_obs(assimilation_cycle)

        dprint('1')
        # get cfract
        CFRACT = np.empty(WH) - 1  # TODO: make domain size a parameter everywhere!
        convert_from_memory(x_b, assimilated_varid, assimilated_index,
                        MyDataType(4, 'float32'), VARID_CFRACT, CFRACT)
        dprint('1.5')
        # some  output:

        for vid in [VARID_T2]: #, VARID_CFRACT, VARID_Q2]:  #, VARID_U_2, VARID_V_2, VARID_W_2]:  those 3 need different shapes! maybe they have more /less dimensions?  FIXME: figure out dimensioins
            print(f'Trying {varid_to_str(vid)}')
            data = np.empty(WH)  # TODO: make domain size a parameter everywhere!
            data[:] = np.nan
            convert_from_memory(x_b, assimilated_varid, assimilated_index,
                            MyDataType(4, 'float32'), vid, data)
            #       y, x
            # more or less Grenoble..., coordinates taken from ncview sample.nc
            wxs = [95, 0, 3, 0, 3]
            wys = [51, 0, 3, 3, 0]

            for wx, wy in zip(wxs, wys):
                if not np.isnan(data[wy,wx]):
                    title = varid_to_str(vid)
                    print(f'{title},{wx},{wy},{assimilation_cycle},{pid},{data[wy,wx]}')
                    # T2[wy,wx] = 1000

                    # save_img(assimilation_cycle, 'T2', T2)

        dprint('2')

        # convert into cloud mask
        split = 0.125
        cldmas = 0*(CFRACT < split) + 1*(CFRACT>=split)
        sq_sum = np.nansum(np.square(obs - cldmas))


        dprint('1')
        save_img(assimilation_cycle, 'pid%d.cldmas' % pid, cldmas)
        if assimilation_cycle > last_cycle:
            last_cycle = assimilation_cycle
            save_img(assimilation_cycle, 'pid%d.obs' % pid, obs)
        dprint('1')

        dprint('1')
        print('sq_sum on rank', rank, ':', sq_sum)
        sq_sums = comm.gather(sq_sum, root=0)
        dprint('1')
        if rank == 0:
            sq_sum = np.sum(sq_sums)   #TODO: maybe normalize by 63*63 (domain size)?
            #w = np.exp((-0.5*exp))
            w = 1/(1+sq_sum)
            print('total weight', pid, ':', w)
    except Exception as e:
        print('[%d] Python Error!' % rank)
        print(e)
        traceback.print_stack()
        traceback.print_exc()
        print('.', flush=True)
        raise e
        exit(1)

    if rank == 0:
        return w
    else:
        return -1
