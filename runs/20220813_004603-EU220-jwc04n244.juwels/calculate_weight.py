from melissa_wrf_helpers import *
from functools import lru_cache

import netCDF4 as NC
import numpy as np

import time

### Taken from Y.S. and adapted
def mask_dtm(NUM, ARR_DTM=[0.0,1.0,2.0], ARR_DTM_RANGE=[0,1], ARR_STR_DTM=["OUT","IN","OUT"]):
    """ Purely determine the classification of a gridcell based on the range of DTM
        ARR_DTM is the classification between the range of ARR_DTM_RANGE, that
        should be less than ARR_DTM
         < 0<= 1 <=2 .. range
         0 | 1 | 2 | 3  classification
        ---+---+---+---
    """
    if np.isnan(NUM):
        return NUM
    else:
        # zip all classes with their upper limit. Since array ranges are sorted in
        # ascending order, we are in the right range as soon as clas is smaller than the
        # upper limit
        for clas, upper_limit in zip(ARR_DTM[:-1], ARR_DTM_RANGE):
            if NUM < upper_limit:
                return clas

        return ARR_DTM[-1]

# nasty unit tests:
assert mask_dtm(0) == 1
assert mask_dtm(0.5) == 1
assert mask_dtm(1) == 2
assert mask_dtm(-1) == 0

def mask_array(ARR_IN):
    """Apply mask_dtm to an array"""

    # note: vectorize will try to convert everything in the same type as the first array element so ensure all are of the same type...
    return np.vectorize(mask_dtm)(ARR_IN)

### end taken from Y.S. and adapted

def assimilation_cycle_to_date_str(ac):
    c = max(ac - 1, 0)  # take first obs file twice, even when it  makes no sense to even load obs for the very first cycle
    return "20180721%02d0000" % c   # ok we match with completely wrong observations for now!

def assimilation_cycle_to_month(assimilation_cycle):
    """Must return a month digit given the current assimilation cycle"""
    # TODO: calculate this better... use env variable containing start date and timesteps from namelist and so on...
    # otherwise there is very high corelation in the software stacks...
    return 8  # start a 9. august, 00:00

def assimilation_cycle_to_hour(assimilation_cycle):
    # TODO: so far only one month...
    # REM: one assimilation cycle takes one hour
    return assimilation_cycle + 9*24 + 12  # as we start the 9th day and we init 12 hours...

def get_obs(assimilation_cycle):
    """Loads cloud fraction observation. Directly returns the masked array for the current
    assimilation cycle. Note that it performs caching. So it will only load the next
    cloud fraction file / generate the fixed mask if this is necessary, (when month / assimilation cycle counts up)"""

    STR_DIR_IN  = "/p/scratch/cjiek80/jiek8010/OUTPUT_DATA"

    # File:                              ... month number ...
    month = assimilation_cycle_to_month(assimilation_cycle)
    if get_obs.month != month:
        # did not memoize this yet...
        # close old dataset if not first timestep
        if get_obs.ds is not None:
            get_obs.d = None
            get_obs.ds.close()

        STR_CLM_FILE     = "EU_220x220_CLMask_2018{0:02d}.nc".format(month)
        get_obs.ds = NC.Dataset("{0:s}/{1:s}".format(STR_DIR_IN, STR_CLM_FILE), "r")
        get_obs.d = get_obs.ds['CLMask_Median']  # REM: this value is either 0 or 1 !

    if get_obs.assimilation_cycle != assimilation_cycle:
        # did not memoize this assimilatino cycle yet
        get_obs.assimilation_cycle = assimilation_cycle
        hour = assimilation_cycle_to_hour(assimilation_cycle)
        try:
            # note that we add 1 to fix the values to our classes
            get_obs.output = get_obs.d[hour, :, :].astype(int)+1  # same as fix array would do: simply convert into int Don't use int8 or int16 as this will map nan's to 0 which is a valid cloud mask value already!
            # np.savetxt(f"obs_{assimilation_cycle}.csv", get_obs.output, delimiter=",")  ## TODO for debugging.

            get_obs.success = (get_obs.output > -10).any()  # there are at least some that are not nan...
        except Exception as e:
            print('[%d] Python Error when trying to return observations!' % rank)
            print(e)
            traceback.print_stack()
            traceback.print_exc()
            print('.', flush=True)
            #raise e
            get_obs.success = False

    return get_obs.success, get_obs.output

# Variables needed for memoization:
get_obs.d = None
get_obs.ds = None
get_obs.output = None
get_obs.assimilation_cycle = -10
get_obs.success = None
get_obs.month = -1


NUM_NX      = 220
NUM_NY      = 220

def calculate_weight(assimilation_cycle, pid, x_b, x_h, assimilated_index, assimilated_varid, fcomm):
    """
        This function is called once on every particle.

        It will:
        1) Calculate diagnostics and write them to disk of this particle (Let's start by writing the cloud mask into a separate file. What is to be done with it can be seen later)
        2) Calculate the weight using the accordance with the cloud mask.
    """

    # Initialing the MPI

    comm = MPI.COMM_WORLD.f2py(fcomm)
    comm_rank = comm.Get_rank()
    comm_size = comm.Get_size()

    # get cfract
    # init al
    CFRACT_IN = np.full((NUM_NX, NUM_NY), np.nan)

    dprint('1')
    convert_from_memory(x_b, assimilated_varid, assimilated_index,
                    MyDataType(4, 'float32'), VARID_CFRACT, CFRACT_IN)
    dprint('222')

    # np.savetxt(f"cfract_part_{assimilation_cycle}.{pid}.{comm_rank}.csv", CFRACT_IN, delimiter=",")

    dprint('Gathering all CFRACT parts from model ranks')
    # gather from all ranks! only calculate weight on rank 0 so far. I don't think its getting much faster anyway by parallelization? we have only 220*220*50 cells to work with. Loading the inputs takes much longer.. I guess
    CFRACTS_IN = comm.gather(CFRACT_IN, root=0)

    result = -1
    if comm_rank == 0:
        global_cfract_in = np.full((NUM_NX, NUM_NY), np.nan)

        dprint('Setting them together')
        for c in CFRACTS_IN:
            mask = np.isfinite(c)
            global_cfract_in[mask] = c[mask]

        dprint('Storing background cloud fraction map')
        # store it to file for later diagnostics:
        np.savetxt(f"cfract_{assimilation_cycle}.{pid}.csv", global_cfract_in, delimiter=",")

        dprint('Masking background cloud fraction map')

        background_cloud_mask = mask_array( global_cfract_in )

        dprint('Retaining observations...')
        success, observed_cloud_mask = get_obs(assimilation_cycle)
        if success:
            dprint('...success!')

            # np.savetxt(f"background_cloud_mask_{assimilation_cycle}.{pid}.csv", background_cloud_mask, delimiter=",")

            #same = np.count_nonzero(observed_cloud_mask == background_cloud_mask)
            same = np.sum(observed_cloud_mask == background_cloud_mask)
            #same_ratio = same / background_cloud_mask.size()
            result = same
        else:
            dprint('...no success! Skipping this time step!')
            result = -1  # TODO: implement feature that when weight is negative for at least
                         # one do not resample

    dprint('Broadcasting result to all ranks')
    comm.bcast(result, root=0)

    dprint('Returning from calculate_weight, weight=', result)
    return float(result)


        # except Exception as e:
            # print(e)
            # traceback.print_stack()
            # traceback.print_exc()
            # print('.', flush=True)
            # raise e
            # exit(1)
