from calculate_weight import calculate_weight

from melissa_wrf_helpers import *


tmp_data = {}
def print_mean(cycle, x_b, assimilated_varid, assimilated_index):
    global tmp_data
    # 2D variables are easier:
    for varid in [VARID_T2, VARID_Q2, VARID_CFRACT]:
        data = np.zeros((63, 63)) - 1
        convert_from_memory(x_b, assimilated_varid, assimilated_index,
                        MyDataType(4, 'float32'), varid, data)

        # cache data a bit to do further stats over ensemble
        if not varid in tmp_data:
            tmp_data[varid] = []
        tmp_data[varid].append(data)

        s = np.nansum(data[data != -1])
        sums = MPI.COMM_WORLD.gather(s, root=0)
        if MPI.COMM_WORLD.rank == 0:
            s = np.sum(sums)
            s /= 63.**2
            print("Cycle %d, Mean of %s is %f" % (cycle, varid_to_str(varid), s))

    # TODO: ad U, V and W_2


def print_std(cycle):
    # this uses data stored in tmp_data to get the standard deviation over the ensemble!
    for varid in [VARID_T2, VARID_Q2, VARID_CFRACT]:
        if MPI.COMM_WORLD.rank == 0:
            s = np.std(tmp_data[varid], axis=0).mean()
            print("Cycle %d, Mean of STD over ensemble of %s is %f" % (cycle, varid_to_str(varid), s))



"""
This function is called by the melissa server when the ensemble is finished and the ensemble update is performed
"""
def callback(cycle, ensemble_list_background, ensemble_list_analysis,
        ensemble_list_hidden_inout, assimilated_index, assimilated_varid):

    rank = MPI.COMM_WORLD.rank
    print('[%d] p0' % rank)
    print('[%d] p1' % rank)
    try:
        weights = []

        # for print mean:
        global tmp_data
        tmp_data = {}
        ## normally we would need some MPI comm here!
        for pid, x_b in enumerate(ensemble_list_background):
            print_mean(cycle, x_b, assimilated_varid, assimilated_index)

            print('[%d] p4' % rank)

            x_h = 0  # we don't use it so don't transfer it here.
            w = calculate_weight(cycle, pid, x_b, x_h, assimilated_index,
                    assimilated_varid, MPI.COMM_WORLD.py2f())
            if rank == 0:
                weights.append((w, pid))

            print('[%d] p5' % rank)

        print_std(cycle)

        out_particles = []
        if rank == 0:
            sum_weights = sum(map(lambda x: x[0], weights))
            normalized_weights = list(map(lambda x: x[0] / sum_weights, weights))


            print("Normalized Weights this cycle:", normalized_weights)
            pids = list(map(lambda x: x[1], weights))
            out_particles = np.random.choice(pids, size=len(weights), p=normalized_weights)
            print("New Particles:", out_particles)
        MPI.COMM_WORLD.bcast(out_particles, root=0)

        npid = 0  # new particle id
        for op in out_particles:
            np.copyto(ensemble_list_background[op], ensemble_list_analysis[npid])
            # Hidden state not used anymore ;)
            npid += 1

        #TODO: only do this if time is big enough!
        # for x_a in ensemble_list_analysis:
            # np.copyto(ensemble_list_background[best_pid], x_a)

        # for x in ensemble_list_hidden_inout:
            # np.copyto(ensemble_list_hidden_inout[best_pid], x)


        print('[%d] p6' % rank)


        # For debugging: write states to pickle:

        to_store = ("cycle", "ensemble_list_background", "ensemble_list_analysis",
            "ensemble_list_hidden_inout", "assimilated_index", "assimilated_varid")

        data = {}
        lcl = locals()
        for s in to_store:
            data[s] = lcl[s]
        print('[%d] p7' % rank)

        if cycle % 10 == 0:
            print('[%d] p7.1' % rank)
            with open('/tmp/t'+str(cycle)+'r' + str(rank) + '.bin', 'wb') as f:
                pickle.dump(data, f)

            print('[%d] p7.2' % rank)


        print('[%d] p8' % rank)


    except Exception as e:
        print('[%d] Python Error!' % rank)
        print(e)
        traceback.print_stack()
        traceback.print_exc()
        print('.', flush=True)
        raise e
        exit(1)

    print('[%d] p9' % rank)
    print('cycle:', cycle)

    return int(os.getenv('MELISSA_DA_WARMUP_NSTEPS') if cycle == 1 else os.getenv('MELISSA_DA_NSTEPS'))
