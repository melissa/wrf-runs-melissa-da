import sys
from  melissa_wrf_helpers import *
import time

import repex
import os



from melissa_da_study import *

def run():

    MEMBERS = 3

    is_p2p = not('no-p2p' in sys.argv)
    N_RUNNERS = 1
    if is_p2p:
        print('Doing a p2p study')
        sn = os.getenv('SLURM_NNODES')
        if sn:
            N_RUNNERS=1*(int(sn) - 1)  # subtract server node
            os.system("srun bash -c 'rm -rf /tmp/ckpts; mkdir /tmp/ckpts'")  # only if is salloc....
        else:
            N_RUNNERS = 512
        MEMBERS = N_RUNNERS * 5
        # MEMBERS = 128  --> 4 members per runner , 2 runner per node --> 16 nodes for runners --> 17 nodes
        MEMBERS = 4*N_RUNNERS
    else:
        print('Doing a study with central state server')

    print("using %d runners and %d members!" % (N_RUNNERS, MEMBERS))

    print("Rem: if you want to see clouds do at least 20 time steps!")

    os.system('rm -rf STATS/')
    os.system('killall xterm')

    print('clean old local Checkpoints')

    clean_old_stats()
    run_melissa_da_study(
            runner_cmd='./wrf.exe',
            precommand_server='',
            # total_steps=180,  # do 30 time steps to have some clouds!
            total_steps=100*60*3*24,  # do one day
            #total_steps=10,  # do 30 time steps to have some clouds!
            ensemble_size=MEMBERS,
            assimilator_type=ASSIMILATOR_PYTHON,
            # cluster=cluster_selector(),
            cluster=SlurmJuwelsCluster('jiek80'),
            procs_server=10,
            nodes_server=1,
            procs_runner=41,
            nodes_runner=1,
            n_runners=N_RUNNERS,  # TODO: check if multiple runners can produce errors!
            show_server_log=False,
            show_simulation_log=False,
            create_runner_dir=True,
            prepare_runner_dir=make_prd_function(MEMBERS),
            runner_timeout=4*60*60,  # 60 minutes time for debugging!
            # runner_timeout=60,  # 60 minutes time for debugging!
            server_timeout=60*60,
            local_ckpt_dir='/tmp/ckpts',
            #precommand_server='xterm_gdb',
            #precommand_server='valgrind --tool=massif',
            additional_server_env={
                'MELISSA_DA_PYTHON_ASSIMILATOR_MODULE': 'callback',
                # 'MELISSA_DA_NSTEPS': '180', # one timestep is 20 s , 60 minutes between observations... --> 60*3
                # 'MELISSA_DA_NSTEPS': '60', # one timestep is 60 s , 60 minutes between observations... --> 60*3
                # 'MELISSA_DA_NSTEPS': '36', # one timestep is 60 s , 60 minutes between observations... --> 60*3
                'MELISSA_DA_NSTEPS': '150', # one timestep is 24 s , 60 minutes between observations...

                # 'MELISSA_DA_NSTEPS': '1', # one timestep is 60 s , 60 minutes between observations... --> 60*3
                # 'MELISSA_DA_WARMUP_NSTEPS': '360', # one timestep is 60 s = 1 min, we want it to warmup during the first 6 hours -> 6h * 60 min/h
                # 'MELISSA_DA_WARMUP_NSTEPS': '1500', # one timestep is 60 s = 1 min, we want it to warmup during the first 6 hours -> 6h * 60 min/h
                # 'MELISSA_DA_NSTEPS': '1', # speedup
                # 'MELISSA_DA_WARMUP_NSTEPS': '1', # speedup # 'MALLOC_CHECK_': '3',
                # 'MALLOC_PERTURB_': '1'
                },
            additional_env={
                'PYTHONPATH': os.getcwd() + ':' + os.getenv('PYTHONPATH'),  # to find calculate_weight
                'MELISSA_DA_PYTHON_CALCULATE_WEIGHT_MODULE': 'calculate_weight',
                # 'MELISSA_DA_TIMING_REPORT': str(time.time() + 22*60),  # write timing events after how many seconds?
                'MELISSA_DA_TIMING_REPORT': str(time.time() + 3600 + 60 * 20),  # write timing events after how many seconds?
                # 'MELISSA_DA_TIMING_REPORT': str(time.time() + 2*60),  # write timing events after how many seconds?
                },
            walltime='07:30:00',
            is_p2p=is_p2p)


cwd = os.getcwd()
repex.run(
        EXPERIMENT_NAME='EU220',
        INPUT_FILES=[
            #'run.sbatch',
            # 'prepare/namelist.input',
            'migration_varnames',
            'assimilation_varnames',
            'calculate_weight.py',
            'callback.py',
            'melissa_wrf_helpers.py',
            os.path.abspath(cwd+'/../melissa-da/build/CMakeCache.txt'),
            # os.path.abspath(cwd+'/../../FTI/build/CMakeCache.txt'),
            os.path.abspath(cwd+'/../WRF/configure.wrf')
            ],
        GIT_REPOS=[
            # os.path.abspath(cwd+'/../'),
            os.path.abspath(cwd+'/../melissa-da'),
            # os.path.abspath(cwd+'/../../FTI'),
            os.path.abspath(cwd+'/../WRF')
            ],
        experiment_function=run)

