import os
import re
import math
# import matplotlib.pyplot as plt
# import scipy.interpolate
# from scipy.spatial.qhull import Delaunay
import pickle
import traceback
import subprocess

import sys


import random
import numpy as np

from functools import lru_cache
import shutil

# always flush prints directly!
import functools
print = functools.partial(print, flush=True)


import mpi4py
mpi4py.rc(initialize=False, finalize=False)
from mpi4py import MPI


# import matplotlib
# import matplotlib.pyplot as plt
# plt.ioff()
# matplotlib.use('Agg')  # don't rely on displays, only plot into files

# Some helpers:

VARID_CFRACT = 1
VARID_T2 = 2
VARID_Q2 = 3
VARID_U_2 = 4
VARID_V_2 = 5
VARID_W_2 = 6

random.seed(123)

DEBUG = True
if DEBUG:
    dprint = print
else:
    def dprint(*args):
        pass

class MyDataType:
    def __init__(self, size, dtype):
        self.size = size
        self.dtype = dtype


def index_to_coord(shape, idx):
    idx -= 1   # we come from fortran which starts counting at 1...
    out = [0] * len(shape)
    for i, _ in enumerate(shape):
        stride = 1  # how many elements to jump for one jumnp in this dimenstion
        for k in shape[i+1:]:
            stride *= k
        out[i] = idx // stride
        idx = idx % stride
    return out

# unit tests:                    +1 for fortran array indices
assert index_to_coord((63,63), 63 + 1) == [1,0]
assert index_to_coord((63,63), 0  + 1) == [0,0]
assert index_to_coord((63,63), 63*63+62 + 1) == [63,62]



def convert_from_memory(raw, assimilated_varid, assimilated_index, datatype, varid,
                        out):
    """
    out : numpy array of correct shape in which we will copy all the data found
    datatype : MyDataType(size, dtype)

    """
    assert isinstance(out, np.ndarray)
    assert isinstance(out, np.ndarray)

    good_entries = np.where(assimilated_varid == varid)[0]

    # dprint('[] assimilated_index - len, max, min:' % len(assimilated_index), max(assimilated_index), min(assimilated_index))

    # dprint('[] raw - len, max, min:' % len(raw), max(raw), min(raw))  # this throws a segfault!

    if len(good_entries) > 0:
        # indices within good_entries array
        chunk_ends = np.where(good_entries[1:] - good_entries[:-1] > 1)[0]
        chunk_starts = chunk_ends + 1  # after an end will be a new start

        # transform into indices within raw == bytes
        chunk_starts = np.concatenate(([good_entries[0]], good_entries[chunk_starts]))
        chunk_ends = np.concatenate((good_entries[chunk_ends] + 1, [good_entries[-1]+1]))

        # iterate over chunks
        for offset, end in zip(chunk_starts, chunk_ends):
            count = end - offset  # offset and count in bytes
            assert count > 0  # was in a chunk
            assert count % datatype.size == 0  # chunk split at good position
            data = np.frombuffer(raw, dtype=datatype.dtype, offset=offset,
                                 count=count//datatype.size)
            # print('offset', offset, 'count', count, 'ds.size', datatype.size)

#         plt.plot(data)
#         plt.show()

            # copy data into right place in out array!
            for i, d in enumerate(data):
#             if varid == VARID_T2 and d == 0:  # filter out meaningless temperature values
#                 continue
#                 d += 273.15
                idx = assimilated_index[i*datatype.size + offset]
                if idx > 0:
                    coord = index_to_coord(out.shape, idx)
                    out[tuple(coord)] = d

                    # print("copying data (%f) to" % d, tuple(coord))
                # one could assert here that all elements between i + offset and
                # i + offset + datatype.size share the same assimilated_index and
                # the same assimilated_varid
    else:
        print('[] This shoulud not happen! could not find any chunks of varid=%d' % (varid))


def varid_to_str(varid):
    return ['0', 'CFRACT', 'T2', 'Q2', 'U_2', 'V_2', 'W_2'][varid]



def save_img(cycle, what, data):
    # if not 'T2' in what:
    return  # save storage for now!
    # fig = plt.figure()
    # plt.imshow(data, origin='lower')
    # plt.colorbar()
    # plt.savefig('%s.worldrank%dt%d.png' % (what, MPI.COMM_WORLD.rank, cycle))
    # plt.close(fig)


@lru_cache()
def get_platform():
    import socket
    h = socket.gethostname()
    if 'narrenkappe' in h:
        return 'narrenkappe'
    elif 'juwels' in h:
        return 'juwels'
    else:
        return 'jean-zay'

def bash(cmd):
    print('executing', cmd)
    return subprocess.call(["bash", "-c", "%s" % cmd])

def make_prd_function(MEMBERS):

    def prepare_runner_dir():
        """
        function that gets called from within the recently created runner dir before the
        runner is launched
        """

        nonlocal MEMBERS

        print('preparing runner dir %s...' % os.getcwd())
        print('get namelist.input:')

        #bash(f'cp {hdir}/namelist.input .')
        hdir = '/p/project/cjiek80/sebastianF'
        bash(f'cp {hdir}/run2/namelist.input ./namelist.input')  # 220 domain


        # Select a new seed:
        seed = random.randint(100,1e9)
        print("Seeding this namelist with", seed)
        # TODO: do this later:
        # bash("sed -i -e 's/PYTHON_SEED/%d/g' namelist.input" % seed)
        # bash("sed -i -e 's/PYTHON_MEMBERS/%d/g' namelist.input" % MEMBERS)

        print('link in executables:')
        bash(f'ln -s {hdir}/WRF/run/* .')

        print('link in data:')
        # bash(f'ln -s {hdir}/../../data/input_20150823/* .')
        bash(f'ln -s {hdir}/run2/prepare/* .')  # 220 domain

        print('link migration_varnames, assimilation_varnames')
        bash(f'ln -s {hdir}/run2/migration_varnames .')
        bash(f'ln -s {hdir}/run2/assimilation_varnames .')

        bash(f'rm rsl.*')  # remove accidently linked in rsl files from other folders

        # not necessary:
        # print('execute real.exe to generate input for wrf.exe')
        # bash('./real.exe')


        print('runnerdir %s is ready' % os.getcwd())

    return prepare_runner_dir
