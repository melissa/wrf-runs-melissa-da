import sys
from  melissa_wrf_helpers import *
import time

import repex
import os



from melissa_da_study import *

def run():

    MEMBERS = 63*5

    is_p2p = not('no-p2p' in sys.argv)
    N_RUNNERS = 1
    if is_p2p:
        print('Doing a p2p study')
        sn = os.getenv('SLURM_NNODES')
        if sn:
            N_RUNNERS=(int(sn) - 1)  # subtract server node
            os.system("srun bash -c 'rm -rf /dev/shm/ckpts; mkdir /dev/shm/ckpts'")  # only if is salloc....
        else:
            N_RUNNERS = 512
        MEMBERS = 5*N_RUNNERS
    else:
        print('Doing a study with central state server')

    print("using %d runners and %d members!" % (N_RUNNERS, MEMBERS))

    print("Rem: if you want to see clouds do at least 20 time steps!")

    os.system('rm -rf STATS/')
    os.system('killall xterm')

    print('clean old local Checkpoints')

    clean_old_stats()
    run_melissa_da_study(
            runner_cmd='/gpfsscratch/rech/moy/rkop006/wrf2/wrf-run/particle-filter-python-big/runner.sh',
            precommand_server='',
            # total_steps=180,  # do 30 time steps to have some clouds!
            total_steps=50000,  # do one day
            #total_steps=10,  # do 30 time steps to have some clouds!
            ensemble_size=MEMBERS,
            assimilator_type=ASSIMILATOR_PYTHON,
            cluster=SlurmCluster("igf@cpu", max_ranks_per_node=40),
            procs_server=10,
            nodes_server=1,
            procs_runner=40 if get_platform() == 'jean-zay' else 3,
            nodes_runner=1,
            n_runners=N_RUNNERS,  # TODO: check if multiple runners can produce errors!
            show_server_log=False,
            show_simulation_log=False,
            create_runner_dir=True,
            prepare_runner_dir=make_prd_function(MEMBERS),
            runner_timeout=10*60,  # 60 minutes time for debugging!
            # runner_timeout=60,  # 60 minutes time for debugging!
            server_timeout=15*60,
            local_ckpt_dir='/dev/shm/ckpts',
            #precommand_server='xterm_gdb',
            #precommand_server='valgrind --tool=massif',
            additional_server_env={
                'MELISSA_DA_PYTHON_ASSIMILATOR_MODULE': 'callback',
                # 'MELISSA_DA_NSTEPS': '1', # one timestep is 60 s , 60 minutes between observations... --> 60*3
                'MELISSA_DA_NSTEPS': '36', # one timestep is 60 s , 60 minutes between observations... --> 60*3
                # 'MELISSA_DA_TEST_FIFO': 'live_timing_events.csv',
                # 'MELISSA_DA_WARMUP_NSTEPS': '1', # speedup
                # 'MALLOC_CHECK_': '3',
                # 'MALLOC_PERTURB_': '1'
                },
            additional_env={
                'PYTHONPATH': os.getcwd() + ':' + os.getenv('PYTHONPATH'),  # to find calculate_weight
                'MELISSA_DA_PYTHON_CALCULATE_WEIGHT_MODULE': 'calculate_weight',
                'MELISSA_DA_TIMING_REPORT': str(int(time.time() + 8*60)),  # write timing events after how many seconds?
                # 'MELISSA_DA_TIMING_REPORT': str(time.time() + 45*60),  # write timing events after how many seconds?
                # 'MELISSA_DA_TIMING_REPORT': str(time.time() + 10*60),  # write timing events after how many seconds?
                },
            walltime='04:30:00',
            is_p2p=is_p2p)


cwd = os.getcwd()
repex.run(
        EXPERIMENT_NAME='big-wrf-particle-filter',
        INPUT_FILES=[
            'run.sbatch',
            'namelist.input',
            'migration_varnames',
            'assimilation_varnames',
            'calculate_weight.py',
            'callback.py',
            'melissa_wrf_helpers.py',
            os.path.abspath(cwd+'/../../melissa-da/build/CMakeCache.txt'),
            os.path.abspath(cwd+'/../../melissa-da/melissa/build/CMakeCache.txt'),
            # os.path.abspath(cwd+'/../../FTI/build/CMakeCache.txt'),
            os.path.abspath(cwd+'/../../WRF/configure.wrf')
            ],
        GIT_REPOS=[
            os.path.abspath(cwd+'/../'),
            os.path.abspath(cwd+'/../../melissa-da'),
            # os.path.abspath(cwd+'/../../FTI'),
            os.path.abspath(cwd+'/../../WRF')
            ],
        experiment_function=run)

